#!/usr/bin/env python

import click


def minutes_to_hms(source_minutes):
    source_seconds = source_minutes * 60

    seconds = (source_seconds % (60 * 60)) % 60
    minutes = (source_seconds % (60 * 60)) / 60
    hours = source_seconds / 3600

    return "%02d:%02d:%02d" % (hours, minutes, seconds)

CONTEXT_SETTINGS = dict(help_option_names=['-h', '--help'])


@click.command(context_settings=CONTEXT_SETTINGS)
@click.option('-D', '--debug/--no-shout', default=False)
@click.option('-d', '--duration', default=None)
@click.option('-b', '--break-length', default=5, type=int)
@click.option('--increase-work-by', default=5, type=int)
@click.option('--initial-work-period', default=5, type=int)
@click.option('--times', default=10, type=int)
def calculate(debug, times, duration, initial_work_period, break_length,
              increase_work_by):
    total_worked_minutes = 0
    total_break_minutes = 0
    total_elapsed_minutes = 0
    minutes_remaining = None

    work_length = initial_work_period

    if duration:
        times = 99

        # Convert hours to minutes
        if duration.endswith('h'):
            duration = int(duration[:-1]) * 60
        elif duration.endswith('m'):
            duration = int(duration[:-1])
        else:
            duration = int(duration)

    if debug:
        pass

    for i in range(1, times + 1):
        total_worked_minutes = total_worked_minutes + work_length
        total_break_minutes = total_break_minutes + break_length
        total_elapsed_minutes = total_worked_minutes + total_break_minutes

        print "%02d:  Work:%02dm  Break:%02dm  " \
            "TOTALS: Worked:%s  Break:%s  Elapsed:%s" % (
                i, work_length, break_length,
                minutes_to_hms(total_worked_minutes),
                minutes_to_hms(total_break_minutes),
                minutes_to_hms(total_elapsed_minutes))

        if duration:
            minutes_remaining = duration - total_elapsed_minutes

            if minutes_remaining > 0:
                work_length = work_length + increase_work_by

                if minutes_remaining > (work_length + break_length):
                    continue
                elif minutes_remaining >= work_length:
                    break_length = minutes_remaining - work_length
                elif minutes_remaining < work_length:
                    work_length = minutes_remaining
                    break_length = 0

            else:
                # No time remaining
                break

        else:
            # Using cycle count, not duration
            work_length = work_length + increase_work_by


if __name__ == '__main__':
    calculate()
