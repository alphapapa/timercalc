#!/usr/bin/clisp

(defun convert-to-hms (time)
  (let ((seconds (mod (mod time (* 60 60)) 60))
        (minutes (floor (mod time (* 60 60)) 60))
        (hours (floor time 3600)))
    (format nil "~2,'0D:~2,'0D:~2,'0D" hours minutes seconds)))

(let ((increase-by-minutes 5)
      (break-minutes 20)
      (work-minutes-initial 5)
      (times 10)
      (work-length 0)
      (break-length 10)
      (total-worked 0)
      (total-break 0)
      (total-elapsed 0))

  (dotimes (count times)
    (setq work-length (+ work-length increase-by-minutes))
    (setq break-length break-minutes)
    (setq total-worked (+ total-worked work-length))
    (setq total-break (+ total-break break-length))
    (setq total-elapsed (+ total-worked total-break))

    (format t "~2D:  Work length:~2,'0dm  Break length:~Dm  TOTALS: Worked:~D  Break:~D  Elapsed:~D~%"
            count work-length break-length
            (convert-to-hms (minutes-to-seconds total-worked))
            (convert-to-hms (minutes-to-seconds total-break))
            (convert-to-hms (minutes-to-seconds total-elapsed)))))

;; ----------------

(defun hms-to-seconds (hms)
  (let* ((time-parts (split-string hms ":"))
         (hours (string-to-number (if (= 3 (length time-parts))
                                      (car time-parts)
                                    "0")))
         (minutes (string-to-number (if (= 3 (length time-parts))
                                        (cadr time-parts)
                                      (car time-parts))))
         (seconds (string-to-number (car (last time-parts)))))
    (+ seconds (* 60 (+ minutes (* 60 hours))))))

(defun duration-from-timestamps (start stop)
  (let ((start-seconds (hms-to-seconds start))
        (stop-seconds (hms-to-seconds stop)))
    (convert-to-hms (- stop-seconds start-seconds))))

(defun hours-to-seconds (hours)
  (* hours 60 60))
(defun minutes-to-seconds (minutes)
  (* minutes 60))
(defun seconds-to-hours (seconds)
  (floor seconds 3600))
(defun seconds-to-minutes (seconds)
  (floor seconds 60))

(defun convert-to-hms* (time)
  (let* ((hours (seconds-to-hours time))
         (minutes (seconds-to-minutes (- time (hours-to-seconds hours))))
         (seconds (- time (minutes-to-seconds minutes) (hours-to-seconds hours))))
    (format nil "~02D:~02D:~02D" hours minutes seconds)))
