#!/bin/bash

increase_work_by_minutes=5
break_minutes_initial=10
work_minutes_initial=5
times=10

total_worked=0
total_break=0
total_elapsed=0

function debug {
    [[ $debug ]] && echo "$@"
}

function minutes_to_hms {
    source_minutes=$1
    source_seconds=$(( source_minutes * 60 ))

    seconds=$(( (source_seconds % (60 * 60)) % 60 ))
    minutes=$(( (source_seconds % (60 * 60)) / 60 ))
    hours=$(( source_seconds / 3600 ))

    printf "%02d:%02d:%02d" $hours $minutes $seconds
}

# ** Args
args=$(getopt -o "db:i:w:t:" -l "debug,break-length:,increase-work-by:,initial-work-period:,times:" -n "pomoinc" -- "$@")
[[ $? -eq 0 ]] || exit 1

eval set -- "$args"

while true
do
    case "$1" in
        -d|--debug)
            debug=true
            ;;
        -b|--break-length)
            shift
            break_minutes_initial=$1
            ;;
        -i|--increase-work-by)
            shift
            increase_work_by_minutes=$1
            ;;
        -w|--initial-work-period)
            shift
            work_minutes_initial=$1
            ;;
        -t|--times)
            shift
            times=$1
            ;;
        --)
            break ;;
    esac

    shift
done

work_length=$work_minutes_initial
break_length=$break_minutes_initial

debug "increase_work_by_minutes:$increase_work_by_minutes"
debug "break_minutes_initial:$break_minutes_initial"
debug "work_minutes_initial:$work_minutes_initial"
debug "times:$times"

for i in $(seq 1 $times)
do
    total_worked=$((total_worked + work_length))
    total_break=$((total_break + break_length))
    total_elapsed=$((total_worked + total_break))

    printf "%02d:  Work:%02dm  Break:%02dm  TOTALS: Worked:%s  Break:%s  Elapsed:%s\n" \
           $i $work_length $break_length \
           $(minutes_to_hms $total_worked) $(minutes_to_hms $total_break) $(minutes_to_hms $total_elapsed)

    work_length=$((work_length + increase_work_by_minutes))
done
